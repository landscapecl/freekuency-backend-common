package dao

import (
	"context"
	"testing"
)

type TestObj struct {
	Cid  string
	Desc string
}

func TestGetInstance(t *testing.T) {
	conn := GetInstance()

	print("Connection ok :", conn)
}

type Hero struct {
	Name   string `json:"name"`
	Alias  string `json:"alias"`
	Signed bool   `json:"signed"`
}

func TestConnectToDB(t *testing.T) {

	db, err := GetInstance().ConnectToDB()
	if err != nil {
		println("[ Connection Error ] :", err)
	}
	println("[ Connection OK ] :", db)
	collection := db.Collection("heroes")
	hero := Hero{}
	result, errorDB := collection.InsertOne(context.Background(), hero)

	if errorDB != nil {
		print("Error DB : ", errorDB)
	}

	println("[ Insert ] : ", result)

}
