package errors

// Registry
const ExistUserName = "WRONG_USER_NAME"
const ExistUserNameDesc = "The username exist"

const PassLength = "WRONG_PASS_LENGTH"
const PassLengthDesc = "The password length must be greater than or equal to 8"

const PassContentUpper = "WRONG_PASS_CONTENT_U"
const PassContentUpperDesc = "The password must be contains at least a upper case char"

const PassContentLower = "WRONG_PASS_CONTENT_L"
const PassContentLowerDesc = "The password must be contains at least a  lowercase case char"

const PassContentDigit = "WRONG_PASS_CONTENT_D"
const PscDigitDesc = "The password must be contains at least a number"

const EmailFormat = "WRONG_EMAIL_FORMAT"
const EmailFormatDesc = "The email field should be a valid email address!"

const EmailExit = "WRONG_EMAIL"
const EmailExitDesc = "The email entered already exists"

const UsernameEmpty = "EMPTY_USERNAME"
const UsernameEmptyDesc = "Username cannot be empty"

const PasswordEmpty = "EMPTY_PASSWORD"
const PasswordEmptyDesc = "Password cannot be empty"

// Recover

const ExternalAccount = "EXTERNAL_ACCOUNT"
const ExternalAccountDesc = "We are sorry, you must contact your registration provider"

const StatusBadRequest = 400
const StatusUnauthorized = 401
const StatusForbidden = 403
const StatusNotFound = 404
const StatusInternalServerError = 500
