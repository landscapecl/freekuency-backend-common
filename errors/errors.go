package errors

type error interface {
	Error() string
}

// errorString is a trivial implementation of error.
type errorString struct {
	s string
}

func New(text string) error {
	return &errorString{text}
}

func (e *errorString) Error() string {
	return e.s
}

type ValueError struct {
	Code      string
	ErrorForm string
}

type SeverError struct {
	Code      string
	ErrorForm string
}

func (e *SeverError) Error() string {
	return e.ErrorForm
}

func (e *ValueError) Error() string {
	return e.ErrorForm
}

type LoginError struct {
	Code      string
	ErrorForm string
}

func (e *LoginError) Error() string {
	return e.ErrorForm
}
