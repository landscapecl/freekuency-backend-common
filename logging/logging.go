package logging

import (
	"fmt"
	"io"
	"os"

	"github.com/sirupsen/logrus"
	easy "github.com/t-tomalak/logrus-easy-formatter"
)

type StandardLogger struct {
	*logrus.Logger
}

func NewLogger(msName string) *StandardLogger {

	logFile := msName + ".log"
	var file, err = os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("Could Not Open Log File : " + err.Error())
	}

	w := io.MultiWriter(os.Stdout, file)

	var baseLogger = logrus.New()
	var standardLogger = &StandardLogger{baseLogger}

	standardLogger.SetOutput(w)
	standardLogger.Formatter = &easy.Formatter{
		TimestampFormat: "2006-01-02 15:04:05",
		LogFormat:       " %time% - [%lvl%] - [%func%] - %msg% \n",
	}

	return standardLogger
}

// File instance a new logrus.Logger. This logger generates stdout and file.
func File(msName string) *logrus.Logger {

	logFile := msName + ".log"
	var file, err = os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("Could Not Open Log File : " + err.Error())
	}

	w := io.MultiWriter(os.Stdout, file)

	var baseLogger = logrus.New()

	baseLogger.SetOutput(w)
	baseLogger.Formatter = &easy.Formatter{
		TimestampFormat: "2006-01-02 15:04:05",
		LogFormat:       "%time% - [%lvl%] - [%func%] - [%username%] - %msg% \n",
	}

	return baseLogger
}

// Stdout instance a logger than generates log to stdout
func Stdout() *logrus.Logger {

	w := io.MultiWriter(os.Stdout)

	var baseLogger = logrus.New()

	baseLogger.SetOutput(w)
	baseLogger.Formatter = &easy.Formatter{
		TimestampFormat: "2006-01-02 15:04:05",
		LogFormat:       "%time% - [%lvl%] - [%func%] - [%username%] - %msg% \n",
	}

	return baseLogger
}
