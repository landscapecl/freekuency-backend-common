package logging

import "testing"

func TestLogwrapper(t *testing.T) {

	log := NewLogger("Test")

	log.WithField("func", "TestLogwrapper").Info("User : Test")
}
