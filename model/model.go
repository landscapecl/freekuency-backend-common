package model

import (
	"github.com/dgrijalva/jwt-go"
)

type MessageResponse struct {
	Code    string `json:"code" example:"00"`
	Message string `json:"message" example:"Message Description"`
}

func NewMessageResponse(code string, msg string) *MessageResponse {
	m := MessageResponse{code, msg}
	return &m
}

type jwtCustomClaims struct {
	Name  string `json:"name"`
	Admin bool   `json:"admin"`
	jwt.StandardClaims
}

type JsonOauthToken struct {
	AccessToken string `json:"access_token" example:"00"`
	IdToken     string `json:"id_token" example:"00"`
	Scope       string `json:"scope" example:"00"`
	Expires     int    `json:"expires_in" example:"00"`
	Type        string `json:"token_type"`
}

type JsonTokenHeader struct {
	Type string `json:"typ"`
	Cryp string `json:"alg"`
	Kid  string `json:"kid"`
}

type SecurityContext struct {
	Data string `json:"data"`
}

type CustomClaims struct {
	Scope    string   `json:"scope,omitempty"`
	Audience []string `json:"aud,omitempty"`
	Is       string   `json:"iss,omitempty"`
	Cid      string   `json:"sub,omitempty"`
}

type Jwks struct {
	Keys []JSONWebKeys `json:"keys"`
}

type JSONWebKeys struct {
	Kty string   `json:"kty"`
	Kid string   `json:"kid"`
	Use string   `json:"use"`
	N   string   `json:"n"`
	E   string   `json:"e"`
	X5c []string `json:"x5c"`
}

type Block struct {
	Try     func()
	Catch   func(Exception)
	Finally func()
}

type Exception interface{}

func Throw(up Exception) {
	panic(up)
}
