package security

import (
	"context"
	"crypto/subtle"
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	b64 "encoding/base64"

	"bitbucket.org/landscapecl/freekuency-backend-common/model"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/micro/go-micro/metadata"
)

func verifyAudFreekuency(aud []string, cmp string, cmpTwo string, required bool) bool {
	if len(aud) <= 0 || aud[0] == "" {
		return !required
	}

	if (subtle.ConstantTimeCompare([]byte(aud[0]), []byte(cmp)) != 0) && (subtle.ConstantTimeCompare([]byte(aud[1]), []byte(cmpTwo)) != 0) {
		return true
	} else {
		return false
	}

}

func verifyIss(iss string, cmp string, required bool) bool {
	if iss == "" {
		return !required
	}
	if subtle.ConstantTimeCompare([]byte(iss), []byte(cmp)) != 0 {
		return true
	} else {
		return false
	}
}

func ValidateToken(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {

		msgUNAUTHORIZED := model.MessageResponse{"UNAUTHORIZED", "Please provide valid credentials"}

		headers := c.Request().Header
		authToken := headers.Get("Authorization")

		if authToken == "" {
			return c.JSON(http.StatusUnauthorized, msgUNAUTHORIZED)
		}
		splitToken := strings.Split(authToken, " ")
		token := splitToken[1]
		testTokenSplit := strings.Split(token, ".")
		testToken := testTokenSplit[1]
		sDec, _ := b64.StdEncoding.DecodeString(testToken)

		hToken := testTokenSplit[0]
		headerToken, _ := b64.StdEncoding.DecodeString(hToken)

		var response model.CustomClaims
		err := json.Unmarshal([]byte(string(sDec)), &response)

		iss := "https://dev-jx3r-r4a.auth0.com/"
		checkIss := verifyIss(iss, response.Is, false)
		if !checkIss {
			return c.JSON(http.StatusUnauthorized, msgUNAUTHORIZED)
		}
		aud := "https://dev-jx3r-r4a.auth0.com/api/v2/"
		audTwo := "https://dev-jx3r-r4a.auth0.com/userinfo"

		checkAud := verifyAudFreekuency(response.Audience, aud, audTwo, false)

		if !checkAud {

			return c.JSON(http.StatusUnauthorized, msgUNAUTHORIZED)
		}
		cert, err := getPemCert(string(headerToken))

		if err != nil {

			return c.JSON(http.StatusUnauthorized, msgUNAUTHORIZED)
		}
		_, errParse := jwt.ParseRSAPublicKeyFromPEM([]byte(cert))

		if errParse != nil {
			return c.JSON(http.StatusUnauthorized, msgUNAUTHORIZED)
		}

		return next(c)

	}
}

func getPemCert(header string) (string, error) {

	cert := ""
	resp, err := http.Get("https://dev-jx3r-r4a.auth0.com/.well-known/jwks.json")

	if err != nil {
		return cert, err
	}
	defer resp.Body.Close()

	var jwks = model.Jwks{}
	err = json.NewDecoder(resp.Body).Decode(&jwks)

	if err != nil {
		return cert, err
	}
	var token model.JsonTokenHeader
	err = json.Unmarshal([]byte(header), &token)
	if err != nil {
		return cert, err
	}

	for k, _ := range jwks.Keys {
		if token.Kid == jwks.Keys[k].Kid {
			cert = "-----BEGIN CERTIFICATE-----\n" + jwks.Keys[k].X5c[0] + "\n-----END CERTIFICATE-----"
		}
	}

	if cert == "" {
		err := errors.New("Unable to find appropriate key.")
		return cert, err
	}

	return cert, nil
}

func GetSecurityContext(c echo.Context) context.Context {

	headers := c.Request().Header
	authToken := headers.Get("Authorization")

	splitToken := strings.Split(authToken, " ")
	token := splitToken[1]

	ctx := metadata.NewContext(context.Background(), map[string]string{
		"Token": token,
	})

	return ctx
}

func GetCidFromContext(ctx context.Context) (string, error) {

	md, _ := metadata.FromContext(ctx)

	userToken := md["Token"]

	tokenSplit := strings.Split(userToken, ".")
	token := tokenSplit[1]
	sDec, _ := b64.StdEncoding.DecodeString(token)

	var user model.CustomClaims
	err := json.Unmarshal([]byte(string(sDec)), &user)

	if err != nil {
		return "", err
	}

	return user.Cid, nil

}
